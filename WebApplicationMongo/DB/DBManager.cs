﻿using System.Threading;
using MongoDB.Driver;

namespace WebApplicationMongo.DB
{
    internal sealed class DBManager
    {
        private static DBManager _sDbmanager = null;
        private static IMongoClient _client;
        private static IMongoDatabase _database;
        private const string DbName = "serverTest";

        public IMongoClient Client
        {
            get { return _client; }
            set { _client = value; }
        }

        public IMongoDatabase Database
        {
            get { return _database; }
            set { _database = value; }
        }

        private DBManager()
        {
            _client = new MongoClient();
            _database = _client.GetDatabase(DbName);
        }
        public static DBManager GetDbManager()
        {
            if (_sDbmanager != null) return _sDbmanager;
            DBManager temp = new DBManager();
            Interlocked.CompareExchange(ref _sDbmanager, temp, null);
            return _sDbmanager;
        }
    }
}