﻿using System;
using WebApplicationMongo.model;

namespace WebApplicationMongo.DB.dao
{
    interface IDao<T, TId> where T : MongoDBEntity
    {
        T Save(T pobject, String colletionName);
        T GetById<T>(TId id, string colletionName);
        User UpdateBuilding(String id, Building building);
        void Delete(TId id, String colletionName);
    }
}
