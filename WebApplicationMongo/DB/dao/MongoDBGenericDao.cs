﻿using System;
using System.Threading;
using MongoDB.Bson;
using MongoDB.Driver;
using WebApplicationMongo.model;

namespace WebApplicationMongo.DB.dao
{
    public class MongoDbGenericDao<T> : IDao<T, string> where T : MongoDBEntity
    {
        //private readonly string _collectioname = typeof(T).Name;

        private static MongoDbGenericDao<T> _mongoDbGenericDao = null;

        private MongoDbGenericDao()
        { }

        public static MongoDbGenericDao<T> GetGenericDaoInstance()
        {
            if (_mongoDbGenericDao != null) return _mongoDbGenericDao;
            MongoDbGenericDao<T> temp = new MongoDbGenericDao<T>();
            Interlocked.CompareExchange(ref _mongoDbGenericDao, temp, null);
            return _mongoDbGenericDao;
        }

        public T Save(T pobject, String colletionName)
        {
            DBManager.GetDbManager().Database.GetCollection<T>(colletionName).InsertOneAsync(pobject);
            return pobject;
        }

        public T GetById<T>(string id, String colletionName)
        {
            var database = DBManager.GetDbManager().Database;
            var collection = database.GetCollection<T>(colletionName);
            var filterId = Builders<T>.Filter.Eq("_id", new ObjectId(id));
            var result = collection.Find(filterId).FirstOrDefault();
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">user id</param>
        /// <param name="building"></param>
        /// <returns></returns>
        public User UpdateBuilding(String id, Building building)
        {
            const string colletionName = "user";
            var filterId = Builders<User>.Filter.Eq("_id", new ObjectId(id));
            var pobject = GetById<User>(id, colletionName);
            if (pobject == null) return null;
            pobject.Buildings.Add(building);
            DBManager.GetDbManager().Database.GetCollection<User>(colletionName).ReplaceOneAsync(filterId, pobject);
            return pobject;
        }

        public void Delete(String id, String colletionName)
        {
            var filterId = Builders<T>.Filter.Eq("_id", new ObjectId(id));
            DBManager.GetDbManager().Database.GetCollection<T>(colletionName).DeleteOneAsync(filterId);
        }
    }
}