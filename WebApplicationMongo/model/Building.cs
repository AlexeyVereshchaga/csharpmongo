﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplicationMongo.controller;

namespace WebApplicationMongo.model
{
    public class Building
    {
        public BuildingType Type { set; get; }
        public Location Location { set; get; }
        public BuildingState State { set; get; }
    }
}