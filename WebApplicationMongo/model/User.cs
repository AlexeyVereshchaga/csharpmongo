﻿using System;
using System.Collections.Generic;

namespace WebApplicationMongo.model
{
    public class User : MongoDBEntity
    {
        public String Name { get; set; }
        public String Email { get; set; }
        public List<Building> Buildings { get; set; }
    }
}