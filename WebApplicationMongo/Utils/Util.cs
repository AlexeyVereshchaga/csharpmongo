﻿using System;
using System.Linq;
using System.Xml;
using WebApplicationMongo.model;

namespace WebApplicationMongo.Utils
{
    public class Util
    {
        public static Building GetBuiding(string xmlStr)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlStr);
            XmlNodeList nodelist = xmlDoc.SelectNodes("building");
            var building = nodelist
                .Cast<XmlNode>()
                .Select(xmlNode => new Building()
                {
                    Type = (BuildingType)Enum.Parse(typeof(BuildingType), xmlNode.SelectSingleNode("type").InnerText, true),
                    Location = xmlNode.SelectNodes("location").Cast<XmlNode>().Select(node => new Location()
                    {
                        X = int.Parse(node.SelectSingleNode("x").InnerText),
                        Y = int.Parse(node.SelectSingleNode("y").InnerText)
                    }).ToList()[0],
                    State = (BuildingState)Enum.Parse(typeof(BuildingState), xmlNode.SelectSingleNode("state").InnerText, true)
                }).ToList()[0];
            return building;
        }
    }
}