﻿using System;
using System.Text;
using System.Web;
using WebApplicationMongo.controller;
using WebApplicationMongo.Utils;

namespace WebApplicationMongo
{
    /// <summary>
    /// Summary description for Handler1
    /// </summary>
    public class Handler1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            var request = context.Request;
            var requestBytes = request.BinaryRead(context.Request.ContentLength);
            var xmlStr = Encoding.UTF8.GetString(requestBytes);

            //Instead of id should be token
            var id = request.QueryString["id"];

            Creator creator = new ConstructionCreator();
            Construction construction = creator.CreateConstruction(id, Util.GetBuiding(xmlStr));
            construction.Build();

            //Write only for testing
            context.Response.ContentType = "text/plain";
            context.Response.Write("test");
        }

        public bool IsReusable => false;
    }
}