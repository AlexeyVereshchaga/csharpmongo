﻿using System;
using WebApplicationMongo.model;

namespace WebApplicationMongo.controller
{
    public abstract class Construction
    {
        public abstract void Build();
    }
}