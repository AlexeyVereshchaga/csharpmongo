﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplicationMongo.model;

namespace WebApplicationMongo.controller
{
    public abstract class Creator
    {
        public abstract Construction CreateConstruction(String id, Building building); //Factory method
    }
}
