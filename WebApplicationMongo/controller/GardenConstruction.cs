﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplicationMongo.DB.dao;
using WebApplicationMongo.model;

namespace WebApplicationMongo.controller
{
    public class GardenConstruction : Construction
    {
        public String Id { set; get; }
        public Building Building { set; get; }

        public GardenConstruction(String id, Building building)
        {
            Id = id;
            Building = building;
        }

        public override void Build()
        {
            //some useful for garden
            MongoDbGenericDao<User>.GetGenericDaoInstance().UpdateBuilding(Id, Building);
        }
    }
}