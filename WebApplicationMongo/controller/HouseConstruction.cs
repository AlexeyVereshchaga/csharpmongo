﻿using System;
using WebApplicationMongo.DB.dao;
using WebApplicationMongo.model;

namespace WebApplicationMongo.controller
{
    public class HouseConstruction : Construction
    {
        public String Id { set; get; }
        public Building Building { set; get; }

        public HouseConstruction(String id, Building building)
        {
            Id = id;
            Building = building;
        }

        public override void Build()
        {
            MongoDbGenericDao<User>.GetGenericDaoInstance().UpdateBuilding(Id, Building);
        }
    }
}