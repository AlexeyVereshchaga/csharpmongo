﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplicationMongo.model;

namespace WebApplicationMongo.controller
{
    public class ConstructionCreator:Creator
    {
        public override Construction CreateConstruction(String id,Building building)
        {
            switch (building.Type)
            {
                    case BuildingType.Garden:
                    return new HouseConstruction(id, building);
                    break;
                    case BuildingType.House:
                    //todo
                    return new HouseConstruction(id, building);
                    break;
                default:
                    throw new ArgumentException("The building type is not recognized.");
            }
        }
    }
}